package com.example.touchlesscontrol


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.Menu
import android.view.accessibility.AccessibilityManager
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.Switch
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.mediapipe.components.PermissionHelper

/**
 * Main activity of MediaPipe example apps.
 * Use Fontawsome icon https://fontawesome.com/v5.15/icons/hand-point-up?style=solid
 */
@SuppressLint("UseSwitchCompatOrMaterialCode")
class MainActivity : AppCompatActivity() {
    companion object {
        init {
            // Load all native libraries needed by the app.
            System.loadLibrary("mediapipe_jni")
            System.loadLibrary("opencv_java3")
        }
    }

    private lateinit var overlayPermissionSwitch : Switch
    private lateinit var cameraPermissionSwitch : Switch
    private lateinit var servicePermissionSwitch : Switch

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        enableToolbar()

        val seekBar = findViewById<SeekBar>(R.id.seekBar)
        val intent = Intent(this, MyAccessibilityService::class.java)
        seekBar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            var progress = 0

            override fun onProgressChanged(seekBar: SeekBar, progressValue: Int, fromUser: Boolean) {
                progress = progressValue
            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }
            override fun onStopTrackingTouch(seekBar: SeekBar) {
                if(progress == 0) return
                intent.putExtra("scale", progress)
                stopService(intent)
                startService(intent)
            }
        })

        overlayPermissionSwitch = findViewById(R.id.overlayPermission)
        cameraPermissionSwitch = findViewById(R.id.cameraPermission)
        servicePermissionSwitch = findViewById(R.id.servicePermission)

        overlayPermissionSwitch.setOnClickListener {
            promptOverLayPermission()
        }
        cameraPermissionSwitch.setOnClickListener {
            PermissionHelper.checkAndRequestCameraPermissions(this)
        }
        servicePermissionSwitch.setOnClickListener {
            promptServicePermission()
        }
        checkPermission()
    }

    override fun onResume() {
        super.onResume()
        checkPermission()
    }

    private fun enableToolbar(){
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return false
    }

    private fun checkPermission() {
        if (!Settings.canDrawOverlays(this)) {
            overlayPermissionSwitch.isChecked = false
        } else {
            overlayPermissionSwitch.isChecked = true
            overlayPermissionSwitch.isClickable = false
        }
        if (!PermissionHelper.cameraPermissionsGranted(this)) {
            cameraPermissionSwitch.isChecked = false
        } else {
            cameraPermissionSwitch.isChecked = true
            cameraPermissionSwitch.isClickable = false
        }
        if (!isServiceEnabled()) {
            servicePermissionSwitch.isChecked = false
        } else {
            servicePermissionSwitch.isChecked = true
            val intent = Intent(this, MyAccessibilityService::class.java)
            stopService(intent)
            startService(intent)
        }
    }

    private fun promptOverLayPermission() {
        val overlayAlert = AlertDialog.Builder(this)
        overlayAlert.setTitle("Overlay Permission")
        overlayAlert.setCancelable(false)
        overlayAlert.setMessage(R.string.overlayAlert)

        overlayAlert.setPositiveButton("OK") { dialog, _ ->
            run {
                val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
                intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                startActivity(intent)
                dialog.cancel()
            }
        }
        overlayAlert.setNegativeButton("Cancel") { dialog, _ -> run {
            overlayPermissionSwitch.isChecked = !overlayPermissionSwitch.isChecked
            dialog.cancel()
        }}
        overlayAlert.show()
    }

    private fun promptServicePermission(){
        val enableServiceAlert = AlertDialog.Builder(this)
        enableServiceAlert.setTitle("Accessibility Permission")
        enableServiceAlert.setCancelable(false)
        enableServiceAlert.setMessage(getString(R.string.requestService, getString(R.string.app_name)))

        enableServiceAlert.setPositiveButton("OK") { dialog, _ ->
            run {
                val intent = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
                intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                startActivity(intent)
                dialog.cancel()
            }
        }
        enableServiceAlert.setNegativeButton("Cancel") { dialog, _ -> run {
            servicePermissionSwitch.isChecked = !servicePermissionSwitch.isChecked
            dialog.cancel()
        }}
        enableServiceAlert.show()
    }

    private fun isServiceEnabled(): Boolean {
        for (enabledService in (getSystemService(ACCESSIBILITY_SERVICE) as AccessibilityManager).getEnabledAccessibilityServiceList(
            -1
        )) {
            val enabledServiceInfo = enabledService.resolveInfo.serviceInfo
            if (enabledServiceInfo.packageName == packageName && enabledServiceInfo.name == MyAccessibilityService::class.java.name) {
                return true
            }
        }
        return false
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("z", "activity destroy")
        val intent = Intent(this, MyAccessibilityService::class.java)
        stopService(intent)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

}