package com.example.touchlesscontrol;

import android.Manifest;
import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.*;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Size;
import android.view.*;
import android.view.accessibility.AccessibilityEvent;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.mediapipe.components.*;
import com.google.mediapipe.formats.proto.LandmarkProto;
import com.google.mediapipe.framework.AndroidAssetUtil;
import com.google.mediapipe.framework.AndroidPacketCreator;
import com.google.mediapipe.framework.Packet;
import com.google.mediapipe.framework.PacketGetter;
import com.google.mediapipe.glutil.EglManager;

import java.util.*;
import java.util.concurrent.ExecutionException;

public class MyAccessibilityService extends AccessibilityService {

    WindowManager.LayoutParams pointerParams = new WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
            WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE |
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
            PixelFormat.TRANSLUCENT
    );

    WindowManager.LayoutParams frameParams = new WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
            WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT
    );

    WindowManager windowManager = null;

    private static final String BINARY_GRAPH_NAME = "hand_tracking_mobile_gpu.binarypb";
    private static final String INPUT_VIDEO_STREAM_NAME = "input_video";
    private static final String OUTPUT_VIDEO_STREAM_NAME = "output_video";
    private static final String OUTPUT_LANDMARKS_STREAM_NAME = "hand_landmarks";
    private static final String INPUT_NUM_HANDS_SIDE_PACKET_NAME = "num_hands";
    private static final int NUM_HANDS = 1;

    private SurfaceTexture previewFrameTexture;
    private SurfaceView previewDisplayView;
    private EglManager eglManager;
    private FrameProcessor processor;
    private ExternalTextureConverter converter;
    private CameraXPreviewHelper cameraHelper;

    private final int previewWidth = 200;
    Handler handler = new Handler(Looper.getMainLooper());
    int rotation = 0;
    int scale = 3;

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED
        ) {
            Toast.makeText(this, "Please allow Camera Permission.", Toast.LENGTH_LONG).show();
        }
        scale = intent.getIntExtra("scale", 3);

        rotation = this.getDisplay().getRotation(); // -90: 3, 0: 0, 90: 1 // bug: from -90 to 90
        if(converter != null) converter.setRotation(rotation); // TODO not update on other apps

        defineCameraProvider();
        if(floatLayout.getParent() == null) this.windowManager.addView(floatLayout, frameParams);
        if(floatFrame.getParent() == null) floatLayout.addView(floatFrame);
        if(this.pointer.getParent() == null) this.windowManager.addView(pointer, pointerParams);
        return START_NOT_STICKY;
    }
    FrameLayout floatFrame;
    LinearLayout floatLayout;

    public void onCreate() {
        this.windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

        createFloatings();

        frameParams.gravity = Gravity.START;
        pointerParams.gravity = Gravity.CENTER;

        createMLInterface();

        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        this.width = displayMetrics.widthPixels;
        this.height = displayMetrics.heightPixels;

        pointer = new TextView(this);
        pointer.setBackgroundResource(R.drawable.point_icon);

        if(pointer.getParent() == null) this.windowManager.addView(pointer, pointerParams);
    }
    TextView pointer;

    ProcessCameraProvider cameraProvider;
    private void defineCameraProvider() {
        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);

        cameraProviderFuture.addListener(() -> {
            try {
                cameraProvider = cameraProviderFuture.get();
            }
            catch (ExecutionException | InterruptedException e) {
                // No errors need to be handled for this Future.
                // This should never be reached.
                Log.e("z", "Camera provider error: " +e);
            }
        }, ContextCompat.getMainExecutor(this));
    }

    void destroy(){
        this.isPreviewing = false;
        cameraProvider.unbindAll();
        if (eglManager != null) eglManager.release();
        if (converter != null) converter.close();

        if(floatLayout.getParent() != null) this.windowManager.removeView(floatLayout);
        if(pointer.getParent() != null) this.windowManager.removeView(pointer);
    }

    public void onDestroy(){
        destroy();
        Log.d("z", "service destroyed");
    }

    @SuppressLint("ClickableViewAccessibility")
    void createFloatings(){
        if(floatFrame != null && floatFrame.getParent() != null) floatLayout.removeView(floatFrame);
        floatLayout = new LinearLayout(this);
        floatLayout.setOrientation(LinearLayout.VERTICAL);
        floatLayout.setBackgroundColor(0x80F7F78C);

        floatFrame = new FrameLayout(this); // frame layout makes preview show easier
        floatFrame.setMinimumWidth(previewWidth);
        //noinspection SuspiciousNameCombination
        floatFrame.setMinimumHeight(previewWidth);

        int closeMinHeight = 20;
        TextView open = new TextView(this);
        open.setText(R.string.open);
        open.setTextColor(Color.BLUE);
        open.setMinimumWidth(previewWidth - 2*closeMinHeight);
        open.setGravity(Gravity.CENTER);
        floatFrameTitle = open;

        TextView close = new TextView(this);
        close.setMinHeight(closeMinHeight);
        close.setMinWidth(2*closeMinHeight);
        close.setText("X");
        close.setTextColor(Color.RED);
        close.setGravity(Gravity.CENTER);
        close.setOnClickListener(e -> {
            AlertDialog.Builder deleteOverlayAlert = new AlertDialog.Builder(this);
            deleteOverlayAlert.setTitle("Remove Overlay");
            deleteOverlayAlert.setCancelable(false);
            deleteOverlayAlert.setMessage(R.string.deleteOverlayAlert);

            deleteOverlayAlert.setPositiveButton("OK", (dialog, a) -> {
                Toast.makeText(this, "Remove Floating Pointer and Camera Preview.", Toast.LENGTH_LONG).show();
                destroy();
                dialog.cancel();
            });
            deleteOverlayAlert.setNegativeButton("Cancel", (dialog, a) ->  dialog.cancel());

            AlertDialog dialog = deleteOverlayAlert.create();
            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
            dialog.show();
        });

        LinearLayout row = new LinearLayout(this);
        row.setGravity(Gravity.END);
        row.addView(open);
        row.addView(close);
        floatLayout.addView(row);

        row.setOnTouchListener((v, e) -> {
            moveFloatView(e);
            return true;
        });
        floatFrame.setOnTouchListener((v, e) -> {
            moveFloatView(e);
            return true;
        });
    }
    TextView floatFrameTitle;

    private float initialTouchX;
    private float initialTouchY;
    private int initialX;
    private int initialY;

    public void moveFloatView(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            initialX = frameParams.x;
            initialY = frameParams.y;
            initialTouchX = event.getRawX();
            initialTouchY = event.getRawY();
            return;
        }
        if (event.getAction() == MotionEvent.ACTION_MOVE) {
            frameParams.x = initialX + ((int) (event.getRawX() - initialTouchX));
            frameParams.y = initialY + ((int) (event.getRawY() - initialTouchY));
            windowManager.updateViewLayout(floatLayout, frameParams);
            return;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if(isPreviewing) return;
            Toast.makeText(this, "Wait 1 second please.", Toast.LENGTH_LONG).show();
            Timer timer = new Timer();
            timer.schedule(new TimerTask(){
                @Override
                public void run() {
                    try {
                        if(isPreviewing) {
                            timer.cancel();
                            handler.post(() -> floatFrameTitle.setText(R.string.drag));
                            return;
                        }
                        handler.post(() -> {
                            createMLInterface();
                            resume();
                        });
                    } catch(RuntimeException e){
                        Log.d("z", e.getMessage());
                    }
                }
            }, 0, 1000);
        }
    }

    void createMLInterface(){
        if(previewDisplayView != null && previewDisplayView.getParent() != null) floatFrame.removeView(previewDisplayView);
        previewDisplayView = new SurfaceView(this);

        eglManager = new EglManager(null);
        try{
            AndroidAssetUtil.initializeNativeAssetManager(this);
        } catch(UnsatisfiedLinkError e){
            Log.d("z", e.getMessage());
        }
        processor =
                new FrameProcessor(
                        this,
                        eglManager.getNativeContext(),
                        BINARY_GRAPH_NAME,
                        INPUT_VIDEO_STREAM_NAME,
                        OUTPUT_VIDEO_STREAM_NAME);
        processor
                .getVideoSurfaceOutput()
                .setFlipY(true);

        AndroidPacketCreator packetCreator = processor.getPacketCreator();
        Map<String, Packet> inputSidePackets = new HashMap<>();
        inputSidePackets.put(INPUT_NUM_HANDS_SIDE_PACKET_NAME, packetCreator.createInt32(NUM_HANDS));
        processor.setInputSidePackets(inputSidePackets);

        processor.addPacketCallback(
                OUTPUT_LANDMARKS_STREAM_NAME,
                (packet) -> {
                    List<LandmarkProto.NormalizedLandmarkList> multiHandLandmarks =
                            PacketGetter.getProtoVector(packet, LandmarkProto.NormalizedLandmarkList.parser());
                    getPoints(multiHandLandmarks);
                });
        handler.post(this::setupPreviewDisplayView);
    }

    boolean isPreviewing = false;
    LandmarkProto.NormalizedLandmark thumbTip;
    LandmarkProto.NormalizedLandmark indexTip;
    double previousDistance = 0;
    void getPoints(List<LandmarkProto.NormalizedLandmarkList> multiHandLandmarks){
        if(!multiHandLandmarks.isEmpty()){
            LandmarkProto.NormalizedLandmark wristPoint = null;
            double constDistance = 0;
            float btnX = 0;
            float btnY = 0;

            for (LandmarkProto.NormalizedLandmarkList landmarks : multiHandLandmarks) {
                int landmarkIndex = -1;
                for (LandmarkProto.NormalizedLandmark landmark : landmarks.getLandmarkList()) {
                    landmarkIndex++;
                    if(landmarkIndex == 0) {
                        wristPoint = landmark;
                        int xFactor = rotation == 0 ? width : height;
                        int yFactor = rotation == 0 ? height : width;
                        btnY = landmark.getY() * yFactor * scale - (float) yFactor * scale / 2;
                        btnX = landmark.getX() * xFactor * scale - (float) xFactor * scale / 2;
                        continue;
                    }
                    if(landmarkIndex == 5) { // index knuckle bone
                        if(wristPoint.isInitialized()){
                            constDistance = Math.pow(wristPoint.getX() - landmark.getX(), 2) +
                                            Math.pow(wristPoint.getY() - landmark.getY(), 2) +
                                            Math.pow(wristPoint.getZ() - landmark.getZ(), 2);
                        }
                        continue;
                    }
                    if(landmarkIndex == 4) {
                        thumbTip = landmark;
                        continue;
                    }
                    if(landmarkIndex == 8) {
                        indexTip = landmark;
                        break;
                    }
                }
            }
            double latestDistance = checkDistance(constDistance);
            if(latestDistance >= previousDistance * 0.8) movePointer((int) btnX, (int) btnY); // to make pointer less shaky when click
            previousDistance = latestDistance;
        }
    }

    void movePointer(int x, int y){
        int shakeOrbital = 20;
        if(Math.pow(pointerParams.x - x, 2) + Math.pow(pointerParams.y - y, 2) < shakeOrbital * shakeOrbital) return;
        pointerParams.x = x;
        pointerParams.y = y;
        try {
            handler.post(() -> windowManager.updateViewLayout(pointer, pointerParams));
        } catch(RuntimeException e){
            Log.d("z", e.getMessage());
        }
    }

    int dispatchTrial = 0;
    int MAX_TRIAL_COUNT = 20;
    public void dispatch(final GestureDescription g) {
        dispatchGesture(g, new AccessibilityService.GestureResultCallback() {
            public void onCompleted(GestureDescription gestureDescription) {
                dispatchTrial = 0;
                Log.d("z", "dispatched");
            }

            public void onCancelled(GestureDescription gestureDescription) {
                if(dispatchTrial++ < MAX_TRIAL_COUNT) {
                    dispatch(g);
                }
            }
        }, null);
    }

    Path path;
    long startTouchTime = 0;
    boolean isTouching = false;
    double checkDistance(double constDistance){
        double distance = Math.pow(thumbTip.getX() - indexTip.getX(), 2) +
                          Math.pow(thumbTip.getY() - indexTip.getY(), 2) +
                          Math.pow(thumbTip.getZ() - indexTip.getZ(), 2);

        int xFactor = rotation == 0 ? width : height;
        int yFactor = rotation == 0 ? height : width;
        float dX = pointerParams.x + (float) xFactor / 2;
        float dY = pointerParams.y + (float) yFactor / 2;
        dX = dX < 0 ? 0 : Math.min(dX, xFactor);
        dY = dY < 0 ? 0 : Math.min(dY, yFactor);
        dY += (float) pointer.getHeight() / 2;
        dX += (float) pointer.getWidth() / 2;
        double[] holdRange = {0.3, 0.7};
        double distanceRatio = Math.sqrt(distance / constDistance);
        boolean changeTouchState = false;

        if(distanceRatio < holdRange[0]){
            if(!isTouching){
                path = new Path();
                path.moveTo(dX, dY);
                startTouchTime = System.currentTimeMillis();
                isTouching = true;
                changeTouchState = true;
            }
            path.lineTo(dX, dY);
        }
        if(distanceRatio > holdRange[1]){
            if(isTouching){
                GestureDescription.Builder builder = new GestureDescription.Builder();
                long duration = System.currentTimeMillis() - startTouchTime;
                long clickMinDuration = 300;
                if(duration < clickMinDuration){
                    click(dX, dY);
                } else {
                    builder.addStroke(new GestureDescription.StrokeDescription(path, 0, duration, false));
                    dispatch(builder.build());
                }
                isTouching = false;
                changeTouchState = true;
            }
        }

        /*long clickMinDuration = 300;
        if(distanceRatio < holdRange[0]){ // TODO make it continuous
            if(!isTouching){
                path = new Path();
                startX = dX;
                startY = dY;
                path.moveTo(dX, dY);
                startTouchTime = System.currentTimeMillis();
                isTouching = true;
                changeTouchState = true;
            }
            path.lineTo(dX, dY);
            long duration = System.currentTimeMillis() - startTouchTime;
            boolean dispatchCondition = duration > clickMinDuration;
//            boolean dispatchCondition2 = Math.pow(startX - dX, 2) + Math.pow(startY - dY, 2) > 500;
            if(dispatchCondition){
                GestureDescription.Builder builder = new GestureDescription.Builder();
                builder.addStroke(new GestureDescription.StrokeDescription(path, 0, duration, true));
                dispatch(builder.build());
                path = new Path();
                path.moveTo(dX, dY);
                startTouchTime = System.currentTimeMillis();
            }
        }
        if(distanceRatio > holdRange[1]){
            if(isTouching){
                GestureDescription.Builder builder = new GestureDescription.Builder();
                long duration = System.currentTimeMillis() - startTouchTime;
                if(duration < clickMinDuration){
                    click(dX, dY);
                } else {
                    builder.addStroke(new GestureDescription.StrokeDescription(path, 0, duration, false));
                    dispatch(builder.build());
                }
                isTouching = false;
                changeTouchState = true;
            }
        }*/

        if(!changeTouchState) return distance;

        try {
            handler.post(() -> pointer.setBackgroundResource(isTouching ? R.drawable.grab_icon : R.drawable.point_icon));
        } catch(RuntimeException e){
            Log.d("z", e.getMessage());
        }
        return distance;
    }

    private void click(float dX, float dY) {
        GestureDescription.Builder builder = new GestureDescription.Builder();
        Path clickPoint = new Path();
        clickPoint.moveTo(dX, dY);
        clickPoint.lineTo(dX, dY);
        builder.addStroke(new GestureDescription.StrokeDescription(clickPoint, 0, 100, false));
        dispatch(builder.build());
    }

    private void setupPreviewDisplayView() {
        previewDisplayView.setVisibility(View.GONE);

        floatFrame.addView(previewDisplayView, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));

        SurfaceHolder previewDisplayViewHolder = previewDisplayView.getHolder();
        //noinspection SuspiciousNameCombination
        previewDisplayViewHolder.setFixedSize(previewWidth, previewWidth);

        if(floatLayout.getParent() == null) this.windowManager.addView(floatLayout, frameParams);
        if(floatFrame.getParent() == null) floatLayout.addView(floatFrame, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        previewDisplayViewHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                processor.getVideoSurfaceOutput().setSurface(holder.getSurface());
                previewFrameTexture.updateTexImage();
                if(!isPreviewing){
                    isPreviewing = true;
                    Intent i = new Intent(Intent.ACTION_MAIN);
                    i.addCategory(Intent.CATEGORY_HOME);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);

                }
                Log.d("z", "surface created");
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                onPreviewDisplaySurfaceChanged(width, height);
                Log.d("z", "surface changed");
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                Log.d("z", "surface destroy");
                processor.getVideoSurfaceOutput().setSurface(null);
                previewDisplayViewHolder.removeCallback(this);
            }
        });
    }

    protected Size computeViewSize(int width, int height) {
        return new Size(width, height);
    }

    int width;
    int height;
    protected void onPreviewDisplaySurfaceChanged(int width, int height) {
        Size viewSize = computeViewSize(width, height);
        Size displaySize = cameraHelper.computeDisplaySizeFromViewSize(viewSize);
        boolean isCameraRotated = cameraHelper.isCameraRotated();

        int w = displaySize.getWidth();
        int h = displaySize.getHeight();
        try{
            if(previewFrameTexture == null) Log.d("z", "preview frame null");
            else converter.setSurfaceTextureAndAttachToGLContext(
                previewFrameTexture,
                isCameraRotated ? h : w,
                isCameraRotated ? w : h);
            previewFrameTexture.updateTexImage();
        } catch(RuntimeException e){
            Log.d("z", e.getMessage());
        }
    }


    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {}

    public void onServiceConnected() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
    }

    void resume() {
        converter = new ExternalTextureConverter(eglManager.getContext(), 2);
        converter.setFlipY(true);
        converter.setConsumer(processor);
        startCamera();
    }

    void startCamera() {
        cameraHelper = new CameraXPreviewHelper();
        cameraHelper.setOnCameraStartedListener((s) -> {
            previewFrameTexture = s;
            if(previewFrameTexture != null) previewFrameTexture.updateTexImage();
            previewDisplayView.setVisibility(View.VISIBLE);
        });

        CustomLifecycle lifecycle = new CustomLifecycle();
        lifecycle.doOnResume();
        lifecycle.doOnStart();
        Size size = computeViewSize(this.width, this.height);
        try {
            cameraHelper.startCamera(this, lifecycle, CameraHelper.CameraFacing.FRONT, size);
        } catch(NullPointerException e){
            Log.d("z", e.getMessage());
        }
        /*
        java.lang.NullPointerException: Attempt to invoke virtual method 'boolean android.util.Size.equals(java.lang.Object)' on a null object reference
        at com.google.mediapipe.components.CameraXPreviewHelper.onInitialFrameReceived(CameraXPreviewHelper.java:359)
        at com.google.mediapipe.components.CameraXPreviewHelper.lambda$startCamera$0$CameraXPreviewHelper(CameraXPreviewHelper.java:209)
        at com.google.mediapipe.components.-$$Lambda$CameraXPreviewHelper$cgPdHxbzKJdwsUMDozJ3PffIK6A.onFrameAvailable(Unknown Source:6)
        at android.graphics.SurfaceTexture$1.handleMessage(SurfaceTexture.java:211)
        at android.os.Handler.dispatchMessage(Handler.java:107)
        at android.os.Looper.loop(Looper.java:238)
        at android.os.HandlerThread.run(HandlerThread.java:67)
         */
    }

    public void onInterrupt() {
        Log.d("z", "interrupt");
    }
}

class CustomLifecycle implements LifecycleOwner {

    private final LifecycleRegistry mLifecycleRegistry;
    CustomLifecycle() {
        mLifecycleRegistry = new LifecycleRegistry(this);
        mLifecycleRegistry.setCurrentState(Lifecycle.State.CREATED);
    }

    void doOnResume() {
        mLifecycleRegistry.setCurrentState(Lifecycle.State.RESUMED);
    }

    void doOnStart() {
        mLifecycleRegistry.setCurrentState(Lifecycle.State.STARTED);
    }

    @NonNull
    public Lifecycle getLifecycle() {
        return mLifecycleRegistry;
    }
}